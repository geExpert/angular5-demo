import {ChangeDetectionStrategy, Component, DoCheck, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CounterComponent implements OnInit, OnChanges, DoCheck {

  @Input()
  count = 0;

  @Output()
  counterModified = new EventEmitter<number>();

  constructor() {
    console.log('CounterComponent.constructor');
  }

  onTimeOut() {
     this.count += 1000;
    setTimeout(() => {
      this.onTimeOut();
    }, 1000);
  }

  ngOnInit() {
    console.log('CounterComponent.ngOnInit');
    // this.onTimeOut();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(`changed count: ${changes.count.currentValue}`);
    console.log(`CounterComponent.ngOnChanges()`, changes);
  }

  increment(event) {
    console.log(event);
    this.count++;
    this.counterModified.emit(this.count);
  }

  decrement(event) {
    this.count--;
    this.counterModified.emit(this.count);
  }

  ngDoCheck() {
    console.log('CounterComponent.ngDoCheck');
  }
}
