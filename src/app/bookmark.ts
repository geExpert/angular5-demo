export class Bookmark {
  constructor(public name: string, public url: string, public color: string) {
  }
}
