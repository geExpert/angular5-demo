import {Component, OnInit} from '@angular/core';
import {Bookmark} from './bookmark';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'app';

  mycount = 100;

  bookmark = new Bookmark('Valami', 'http://valami.com', 'green');

  ngOnInit(): void {
  }

  increment() {
    this.mycount++;
  }

  decrement() {
    this.mycount--;
  }

  onCounterChanged(counter: number) {
    this.mycount = counter;
  }

  onBookmarkChanged(changedBookmark: Bookmark) {
    console.log('app component received: ', changedBookmark);
  }
}
