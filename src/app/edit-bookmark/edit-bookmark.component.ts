import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Bookmark} from '../bookmark';
import {FormBuilder, FormGroup} from '@angular/forms';
import 'rxjs/add/operator/map';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-edit-bookmark',
  templateUrl: './edit-bookmark.component.html',
  styleUrls: ['./edit-bookmark.component.css']
})
export class EditBookmarkComponent implements OnInit, OnDestroy {

  @Input()
  bookmark: Bookmark;
  formGroup: FormGroup;
  @Output()
  bookmarkChanged = new EventEmitter<Bookmark>();
  private formValueChangesSubsciption: Subscription;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.formGroup = this.formBuilder.group(
      {
        'nev': this.bookmark.name,
        'url': this.bookmark.url,
        'szin': this.bookmark.color
      });

    this.formValueChangesSubsciption = this.formGroup.valueChanges
      .map(value => new Bookmark(value.nev, value.url, value.szin))
      .subscribe(bookmark => this.bookmarkChanged.next(bookmark));
  }

  ngOnDestroy(): void {
    if (this.formValueChangesSubsciption) {
      this.formValueChangesSubsciption.unsubscribe();
    }
  }


  onSubmit() {
    console.log('submit', this.bookmark);
  }

}
